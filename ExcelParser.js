var oFileIn;
var firstTableEnds;
var remarksPos;
var roomsPos;
const months = {
   "JANUARY":1, 
   "FEBRUARY":2, 
   "MARCH":3, 
   "APRIL":4, 
   "MAY":5, 
   "JUNE":6, 
   "JULY":7, 
   "AUGUST":8, 
   "SEPTEMBER":9,
   "OCTOBER":10, 
   "NOVEMBER":11, 
   "DECEMBER":12, 
}

$(function() {
    oFileIn = document.getElementById('my_file_input');
    if(oFileIn.addEventListener) {
        oFileIn.addEventListener('change', filePicked, false);
    }
});

/****************************
    opens xls, xlsx files
****************************/
function filePicked(oEvent) {
    var oFile = oEvent.target.files[0];
    var sFilename = oFile.name;
   
    var reader = new FileReader();
    
    // Ready The Event For When A File Gets Selected
    reader.onload = function(e) {
        var data = e.target.result;
        var wb = XLSX.read(data, {type: 'binary', cellDates:true, cellStyles:true});

        var sheetTabels = [];
        // Loop Over Each Sheet in xsl or xslx
        wb.SheetNames.forEach(function(sheetName) {
            // Obtain The sheet As CSV
            var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);   
            var oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);  
            var formula = XLS.utils.get_formulae(wb.Sheets[sheetName]);

            var mergesRemark = [];
            var mergesRooms = [];
            var mergesSheet = wb.Sheets[sheetName]['!merges'];

            remarksPos = findCol(formula,'Remarks');
            roomsPos = findCol(formula,'Room');

            var indexOfRemarks = remarksPos.col
            var indexOfRooms = roomsPos.col

            // finding all the mergesRemark col's for Remarks
            for(obj in mergesSheet ){
                if(mergesSheet[obj].s.c == indexOfRemarks && mergesSheet[obj].e.c == indexOfRemarks){
                    mergesRemark.push({row1:(parseInt(mergesSheet[obj].s.r) + 1),row2:(parseInt(mergesSheet[obj].e.r) + 1)});
                }
                if(mergesSheet[obj].s.c == indexOfRooms && mergesSheet[obj].e.c == indexOfRooms){
                    mergesRooms.push({row1:(parseInt(mergesSheet[obj].s.r) + 1),row2:(parseInt(mergesSheet[obj].e.r) + 1)});
                }
            }

            var tables = FirstTable(sCSV) +"<br>"+ SecondTable(sCSV,formula,mergesRemark,mergesRooms);
            
            sheetTabels.push(tables)
            $("#my_file_output").html(tables);

        });
        
        //downloading the excel files
        for (var i=0; i< sheetTabels.length; i++) {
            //saveExcel(sheetTabels[i]);
            saveCsv(sheetTabels[i]);
        }
        //empty the input for new uploding
        $("#my_file_input").val('');
    };
    
    // Tell JS To Start Reading The File.. You could delay this if desired
    reader.readAsBinaryString(oFile);
}
/****************************
    build the First Table
    return First Table
****************************/
function FirstTable(excelAsText){
    //excelAsText = excelAsText.replace(/\s/g, '');
    var hotalIndex = excelAsText.indexOf('HOTEL / ATT');
    hotalIndex = excelAsText.indexOf(':',hotalIndex)+1;
    var groupIndex = excelAsText.indexOf('NAME OF THE GROUP');
    groupIndex = excelAsText.indexOf(':',groupIndex)+1;
    var lastIndexHotel = excelAsText.indexOf(',',hotalIndex);
    var hotalName = excelAsText.substring(hotalIndex,lastIndexHotel);
    var lastIndexGroup = excelAsText.indexOf(',',groupIndex);
    var groupName = excelAsText.substring(groupIndex,lastIndexGroup);
    var table = "\
    <table>\
        <tr>\
            <th style='background-color:rgb(220,230,241);'>Group Name</td>\
            <td>"+groupName+"</td>\
        </tr>\
        <tr>\
            <th style='background-color:rgb(220,230,241);'>Hotal Name</td>\
            <td>"+hotalName+"</td>\
        </tr>\
          <tr>\
            <td> </td>\
            <td> </td>\
        </tr>\
    </table>"

    firstTableEnds = lastIndexHotel>lastIndexGroup?lastIndexHotel:lastIndexGroup;
    return table;
}
/****************************
    build the Second Table
    return Second Table
****************************/
function SecondTable(excelAsText,formula,mergesRemark,mergesRooms){

    var table = "\
    <table>\
        <tr>\
           <th style='background-color:rgb(220,230,241);'>Last Name</th>\
           <th style='background-color:rgb(220,230,241);'>First Name</th>\
           <th style='background-color:rgb(220,230,241);'>Adults</th>\
           <th style='background-color:rgb(220,230,241);'>Juniors</th>\
           <th style='background-color:rgb(220,230,241);'>Children</th>\
           <th style='background-color:rgb(220,230,241);'>Infants</th>\
           <th style='background-color:rgb(220,230,241);'>Share</th>\
           <th style='background-color:rgb(220,230,241);'>Order</th>\
           <th style='background-color:rgb(220,230,241);'>Arrive</th>\
           <th style='background-color:rgb(220,230,241);'>Depart</th>\
           <th style='background-color:rgb(220,230,241);'></th>\
           <th style='background-color:rgb(220,230,241);'></th>\
           <th style='background-color:rgb(220,230,241);'></th>\
           <th style='background-color:rgb(220,230,241);'>Bus Code</th>\
           <th style='background-color:rgb(220,230,241);'>Remarks</th>\
        </tr>"
    var guests = findAllGuests(formula,mergesRemark,mergesRooms); 
    var getDate = getDates(excelAsText);

    var i = 1;
    for(index in guests){
        var guest = guests[index]
        if (guest.firstName!='' && guest.lastName!='') {
            table += "\
            <tr>\
                <td>"+guest.lastName+"</td>\
                <td>"+guest.firstName+"</td>\
                <td>1</td>\
                <td>0</td>\
                <td>0</td>\
                <td>0</td>\
                <td>"+guest.share+"</td>\
                <td>"+guest.excelId+"</td>\
                <td>"+getDate.arrivelDateDay+"</td>\
                <td>"+getDate.dipartlDateDay+"</td>\
                <td></td>\
                <td></td>\
                <td></td>\
                <td></td>\
                <td>"+guest.remarks+". room: "+guest.room+"</td>\
           </tr>"
           i++;
       }
    }
    table += "</table>"

    return table;
}

/****************************
   return arrivelDateDay, dipartlDateDay
****************************/
function getDates(excelAsText){
    var datesIndex = excelAsText.indexOf('DATES:')+'DATES:'.length
    var hyphenIndex = excelAsText.indexOf("-",datesIndex);
    var sleshIndex = excelAsText.indexOf("/",datesIndex);
  
    var lastDatesHotel = excelAsText.indexOf(',',datesIndex);

    var arrivelDateDay;
    var dipartlDateDay; 

    var year = (new Date()).getFullYear();
    //01/05 - 05/05
    if (sleshIndex<hyphenIndex) {
        var arrivelDateDay = excelAsText.substring(datesIndex,hyphenIndex);
        var nextSleshIndex = excelAsText.indexOf("/",sleshIndex+1);
        var dipartlDateDay = excelAsText.substring(hyphenIndex+1,lastDatesHotel); 
        return {arrivelDateDay:arrivelDateDay.replace("/",".")+'.'+year, dipartlDateDay:dipartlDateDay.replace("/",".")+'.'+year}
    }else{
        var arrivelDateDay = excelAsText.substring(datesIndex,hyphenIndex);
        var dipartlDateDay = excelAsText.substring(hyphenIndex+1,hyphenIndex+3); 
    }

    var month='';
    //03-04/06
    if (sleshIndex != -1 && lastDatesHotel > sleshIndex) {
        month = excelAsText.substring(sleshIndex+1,sleshIndex+3);
    }
    //19-20 DECEMBER
    else{
        month = months[excelAsText.substring(hyphenIndex+3,lastDatesHotel).replace(/\s/g,'').toUpperCase()];
    }
    return {arrivelDateDay:arrivelDateDay+'.'+month+'.'+year, dipartlDateDay:dipartlDateDay+'.'+month+'.'+year}
}

/****************************
   return position of remarks
****************************/
function findCol(formula,str){
    for (var i = 0; i < formula.length; i++) {
        if (getFormulaValue(formula[i]).indexOf(str) != -1) {
            return {
                letter:formula[i].substring(0,1), 
                //col:formula[i].substring(1,formula[i].indexOf("=")),
                col:formula[i].substring(0,1).toLocaleLowerCase().charCodeAt(0) - 97,
                index:i
            }
        };
    };
    return null;
}

/****************************
   return al the gusts in excel
****************************/
function findAllGuests(formula,mergesRemark,mergesRooms){
    var guests = {}
      var k = 1;  
      for (var i = 0; i < formula.length; i++) {
        var firstChar = formula[i].substring(0,1);
        if (firstChar == 'A' && isNumeric(getFormulaValue(formula[i]))) { 
            var guest={};
            guest.excelId = getFormulaValue(formula[i]);
            guest.firstName = '';
            guest.lastName = '';
            guest.remarks = '';
            guest.room = '';

            guest.idex = k;
            guest.share = k++;

            var counter = 0;
            do {
                i++
                firstChar = formula[i].substring(0,1);
                switch (firstChar)
                {
                    case "B":
                        guest.firstName = getFormulaValue(formula[i]);
                        counter++;
                        break;
                    case "C":
                        guest.lastName = getFormulaValue(formula[i]);
                        counter++;
                        break;
                    case remarksPos.letter:
                        guest.remarks = getFormulaValue(formula[i]);
                        counter++;
                        break; 
                    case roomsPos.letter:
                        guest.room = getFormulaValue(formula[i]);
                        counter++;
                        break;    
                    default:
                        break;
                }

            }while(firstChar != 'A');
            if (counter>0 && guest.firstName!='') {
                guests[formula[i].substring(1,formula[i].indexOf("=")) - 1] = guest;
            };
            i--;
        };
    };

    for (index in mergesRemark) {
        var merge = mergesRemark[index]
        var i = merge.row1 + 1;
        while(i <= merge.row2 && guests[i]){
            guests[i].remarks = guests[i-1].remarks; 
            i++;
        }
    };
    //order mergesRooms
    mergesRooms.sort(function(a,b){
        return a.row1-b.row1;
    });
    //building th guest share by rooms merge
    var startIndex = 0;
    for (index in guests) {
        if (index < mergesRooms[0].row1) {
            //startIndex = index;
            var roomLowerCase = guests[index].room.toLocaleLowerCase();
            if (roomLowerCase != 'sgl' && roomLowerCase != 'single') {
                startIndex++;
            }
        }else{
            break;
        }
    }
    var temp = startIndex//guests[startIndex].share;//guests[mergesRooms[0].row2].share;
    temp++;
    for (index in mergesRooms) {
        var merge = mergesRooms[index]
        var i = merge.row1 + 1;
        var increseTemp = false;
        while(i <= merge.row2 && guests[i]){
            guests[i].room = guests[i-1].room; 
            var roomLowerCase = guests[i].room.toLocaleLowerCase();
            if (roomLowerCase == 'sgl' || roomLowerCase == 'single') {
                break;
            }
            else if (temp <= guests[i-1].share) {
                guests[i].share = guests[i-1].share = temp;
                increseTemp = true;
            }else if(temp < guests[i].share){
                guests[i].share = temp;
                increseTemp = true;
            }else{
                temp = guests[i].share;
                guests[i].share = guests[i-1].share;
            }

            i++;
        } 
        if (increseTemp) {
            increseTemp = false;
            temp++;
        };
    };
   
    for (index in guests) {
        var roomLowerCase = guests[index].room.toLocaleLowerCase();
        if (roomLowerCase == 'sgl' || roomLowerCase == 'single') {
            guests[index].share = 0;
        }
    }
    return guests;
}

function isNumeric(num){
  return !isNaN(num)
}

/****************************
    get the value of cell in Formula
    "A3 = DATES:   01/05 - 05/05"
****************************/
function getFormulaValue(str){
    if (typeof(str) != "string") {
        return ''
    };
    return str.substring(str.indexOf("=")+1);
}

/****************************
    downloading excel  
****************************/
function saveExcel(tables){
    tables = "<html><head><meta charset='utf-8'><title>a</title></head><body>" + tables + "</body></html>"
    var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
    var ctx = {worksheet: name || 'Worksheet', table: tables}

    //window.location.href = uri + base64(format(template, ctx))
    window.open(uri + base64(format(template, ctx)),'_blank');
}

function saveCsv(table){
       var data, filename, link;
        var csv = export_table_to_csv(table);
        if (csv == null) return;
        filename = 'Import Rooming List.csv';

        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,' + csv;
        }
        data = encodeURI(csv);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
        link.click();
}

function export_table_to_csv(html) {
    var csv = [];
    var parser = new DOMParser();
    var doc = parser.parseFromString(html, "text/html");
    var rows = doc.querySelectorAll("table tr");
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        
        csv.push(row.join(","));        
    }

    return csv.join("\n");
}
